package eg.edu.alexu.csd.filestructure.sort.implementations;

import java.util.ArrayList;
import java.util.Collection;
import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

/**
 * @author AyaOsman And yomnaElDawy
 *.
 * @param <T>
 */
public class MyHeap<T extends Comparable<T>> implements IHeap<T> {
	/**
	 * . nodeList is created to hold all my node in heap.
	 */
	private ArrayList<INode<T>> nodesList =
			new ArrayList<INode<T>>();

	/**
	 *.
	 */
	public MyHeap() {
		INode<T> dummy = new MyNode<T>(this);
		nodesList.add(dummy);
	}

	@Override
	public final INode<T> getRoot() {
		if (nodesList.size() > 1) {
			return nodesList.get(1);
		}
		return null;
	}

	@Override
	public final int size() {
		return nodesList.size() - 1;
	}

	@Override
	public final void heapify(final INode<T> node) {
		INode<T> l = node.getLeftChild();
		INode<T> r = node.getRightChild();
		T intiaValue = node.getValue();
		boolean isLeft = true;
		if (l != null && intiaValue.
				compareTo(l.getValue()) < 0) {
			intiaValue = l.getValue();
		}
		if (r != null && intiaValue.
				compareTo(r.getValue()) < 0) {
			intiaValue = r.getValue();
			isLeft = false;
		}
		if (!(intiaValue.equals(node.getValue()))) {
			if (isLeft) {
				swapValues(node, l);
				heapify(l);
			} else {
				swapValues(node, r);
				heapify(r);
			}
		}
	}

	/**.
	 * @param node
	 .
	 * @param l
	swap value of nodes.
	 */
	private void swapValues(final INode<T> node, final INode<T> l) {
		T temp = node.getValue();
		node.setValue(l.getValue());
		l.setValue(temp);

	}

	@Override
	public final T extract() {
		if (nodesList.size() < 2) {
			return null;
		}
		T root = getRoot().getValue();
		swapValues(nodesList.get(1),
				nodesList.get(nodesList.size() - 1));
		nodesList.remove(nodesList.size() - 1);
		if (nodesList.size() > 1) {
			heapify(getRoot());
		}
		return root;
	}

	@Override
	public final void insert(final T element) {
		MyNode<T> n = new MyNode<T>(this);
		n.setIndex(nodesList.size());
		n.setValue(element);
		nodesList.add(n);
		heapifyUp(nodesList.size() - 1);

	}

	@Override
	public final void build(final Collection<T> unordered) {
		addNodes(unordered);
		int size = unordered.size();
		for (int i = size / 2; i > 0; i--) {
			heapify(nodesList.get(i));
		}

	}

	/**.
	 * @param unordered
	  	make arrayList of node.
	 */
	public final void addNodes(final Collection<T> unordered) {
		Object[] unorderedArray = unordered.toArray();
		nodesList = new ArrayList<INode<T>>();
		MyNode<T> dummy = new MyNode<T>(this);
		dummy.setIndex(0);
		nodesList.add(dummy);
		for (int i = 0; i < unordered.size(); i++) {
			MyNode<T> temp = new MyNode<T>(this);
			temp.setValue((T) unorderedArray[i]);
			temp.setIndex(i + 1);
			nodesList.add(temp);
		}
	}

	/**
	 * @param i
		.
	 */
	public final void heapifyUp(final int i) {
		if (i > 1 && (nodesList.get(i).getValue().
			compareTo(nodesList.get(i / 2).getValue())) > 0) {
			swapValues(nodesList.get(i), nodesList.get(i / 2));
			heapifyUp(i / 2);
		}
	}


	/**.
	 * @param i1
	 .
	 * @param i2
	 .
	 */
	public final void swap(final int i1, final int i2) {
		INode<T> temp = nodesList.get(i1);
		nodesList.set(i1, nodesList.get(i2));
		nodesList.set(i2, temp);
	}

	/**
	 *print array
	 *.
	 */
	public final void print() {
		for (int i = 1; i < nodesList.size(); i++) {
			System.out.println(nodesList.get(i).getValue());
		}
	}

	/**
	 * @return nodeList
	 *.
	 */
	public final ArrayList<INode<T>> getNodes() {
		return this.nodesList;
	}

}
