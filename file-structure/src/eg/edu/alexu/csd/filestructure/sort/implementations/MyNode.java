package eg.edu.alexu.csd.filestructure.sort.implementations;

import java.util.ArrayList;

import eg.edu.alexu.csd.filestructure.sort.INode;

/**
 * @author Target
 *.
 * @param <T>
 */
public class MyNode<T extends Comparable<T>> implements INode<T> {

	/**
	 * @param args
	 * .
	 */
	/**
	 *.
	 */
	private ArrayList<INode<T>> nodes;
	/**
	 *.
	 */
	private T value;
	/**
	 *.
	 */
	private int indexOfArray;

	/**
	 * @param heap
	 *.
	 */
	public MyNode(final MyHeap<T> heap) {
		nodes = heap.getNodes();
	}
	/**
	 * @param index
	  .
	 */
	public final void setIndex(final int index) {
		this.indexOfArray = index;
	}

	@Override
	public final INode<T> getLeftChild() {
		if (nodes.size() > 2 * indexOfArray) {
			return nodes.get(2 * indexOfArray);
		} else {
			return null;
		}
	}

	@Override
	public final INode<T> getRightChild() {
		if (nodes.size() > 2 * indexOfArray + 1) {
			return nodes.get(2 * indexOfArray + 1);
		} else {
			return null;
		}
	}

	@Override
	public final INode<T> getParent() {
		if (indexOfArray / 2 > 1) {
			return nodes.get(indexOfArray / 2);
		} else {
			return null;
		}
	}

	@Override
	public final T getValue() {
		if (nodes.size() > 1) {
		return value;
		} else {
			return null;
		}
	}

	@Override
	public final void setValue(final T valuex) {
		this.value = valuex;
	}

}
