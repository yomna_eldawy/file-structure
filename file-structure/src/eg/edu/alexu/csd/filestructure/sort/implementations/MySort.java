package eg.edu.alexu.csd.filestructure.sort.implementations;

import java.util.ArrayList;
import java.util.Collections;
//import java.util.Random;
import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.ISort;

/**
 * @author Aya Osman
 *.
 * @param <T>
 */
public class MySort<T extends Comparable<T>> implements ISort<T> {
//	private static Random random = new Random();

	@Override
	public final IHeap<T> heapSort(final ArrayList<T> unordered) {
		MyHeap<T> sortedArray = new MyHeap<T>();
		sortedArray.build(unordered);
		ArrayList<T> sort = new ArrayList<T>();
		int size = unordered.size();
		for (int i = size - 1; i >= 0; i--) {
			sort.add(sortedArray.extract());
		}
		Collections.reverse(sort);
		sortedArray.addNodes(sort);
		return sortedArray;
	}
	@Override
	public final void sortSlow(final ArrayList<T> unordered) {
		// using bubble sort
		int n = unordered.size();
		for (int i = 0; i < n; i++) {
			for (int j = 1; j < (n - i); j++) {
				if (unordered.get(j - 1).
			    compareTo(unordered.get(j)) > 0) {
					// swap elements
					swap(unordered, j - 1, j);
				}
			}
		}

	}

	@Override
	public final void sortFast(final ArrayList<T> unordered) {
		//int size = unordered.size();
		//randomizedQuickSort(unordered, 0, size - 1);
		Collections.sort(unordered);

	}

//	private void randomizedQuickSort(ArrayList<T> a, int l, int r) {
//		if (l >= r) {
//			return ;
//		}
//		int k = random.nextInt(r - l + 1) + l;
//		swap(a, l, k);
//		ArrayList<Integer> m = partition3(a, l, r);
//		randomizedQuickSort(a, l, m.get(0) - 1);
//		randomizedQuickSort(a, m.get(1) + 1, r);
//	}
//
//	private ArrayList<Integer>
	//partition3(ArrayList<T> a, int l, int r) {
//		T x = a.get(l);
//		int i = l + 1;
//		for (; i <= r;) {
//			if (x.compareTo(a.get(i)) < 0) {
//				swap(a, r, i);
//				r--;
//
//			} else if (x.compareTo(a.get(i)) > 0) {
//				swap(a, l, i);
//				l++;
//				i++;
//			} else {
//				i++;
//			}
//		}
//		int m1 = l;
//		int m2 = r;
//		ArrayList<Integer> m = new ArrayList<>();
//		m.add(m1);
//		m.add(m2);
//		return m;
//	}
//
	/**
	 * @param target
	 .
	 * @param a1
	 .
	 * @param a2
	 .
	 */
	private void swap(final ArrayList<T> target, final int a1,
			final int a2) {
		T temp1 = target.get(a1);
		T temp2 = target.get(a2);
		target.remove(a1);
		target.add(a1, temp2);
		target.remove(a2);
		target.add(a2, temp1);
	}

}
