package eg.edu.alexu.csd.filestructure.sort.implementations;

import eg.edu.alexu.csd.filestructure.sort.INode;

public class MyNode<T extends Comparable<T>> implements INode<T> {

	/**
	 * @param args
	 */
	private INode<T> leftChild, rightChild, parent;
	private T value;
	private int index;

	@Override
	public INode<T> getLeftChild() {
		return leftChild;
	}

	@Override
	public INode<T> getRightChild() {
		return rightChild;
	}

	@Override
	public INode<T> getParent() {
		return parent;
	}

	public void setLeftChild(INode<T> leftChild) {
		this.leftChild = leftChild;
	}

	public void setRightChild(INode<T> rightChild) {
		this.rightChild = rightChild;
	}

	public void setParent(INode<T> parent) {
		this.parent = parent;
	}

	@Override
	public T getValue() {
		// TODO Auto-generated method stub
		return value;
	}

	@Override
	public void setValue(T value) {
		this.value = value;
	}


}
