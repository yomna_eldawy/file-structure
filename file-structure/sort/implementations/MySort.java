package eg.edu.alexu.csd.filestructure.sort.implementations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.ISort;

public class MySort<T extends Comparable<T>> implements ISort<T> {
	private static Random random = new Random();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<Integer> s = new ArrayList<Integer>();
		s.add(120);
		s.add(19);
		s.add(901);
		MySort<Integer> ss = new MySort<>();
		//ss.sortFast(s);
		ss.sortSlow(s);
		for (int i = 0; i < 3; i++) {
			System.out.println(s.get(i));
		}

	}

	@Override
	public IHeap<T> heapSort(ArrayList<T> unordered) {
		// TODO Auto-generated method stub
		IHeap<T> sortedArray = new MyHeap<T>();
		IHeap<T> sortedArrayCopy = new MyHeap<T>();
		ArrayList<T> orderedList = new ArrayList<T>();
		sortedArray.build(unordered);
		sortedArrayCopy.build(unordered);
		while(sortedArrayCopy.size() >= 1){
			orderedList.add(sortedArrayCopy.extract());
		}
		for (int i = 0 ; i < unordered.size() ; i ++){
			unordered.set(i, orderedList.get(i));
		}
		return sortedArray;
	}

	@Override
	public void sortSlow(ArrayList<T> unordered) {
		// using bubble sort
		int n = unordered.size();
		for (int i = 0; i < n; i++) {
			for (int j = 1; j < (n - i); j++) {
				if (unordered.get(j - 1).compareTo(unordered.get(j)) > 0) {
					// swap elements
					swap(unordered, j - 1, j);
				}

			}
		}

	}

	@Override
	public void sortFast(ArrayList<T> unordered) {
		int size = unordered.size();
		//randomizedQuickSort(unordered, 0, size - 1);
		Collections.sort(unordered);

	}

	private void randomizedQuickSort(ArrayList<T> a, int l, int r) {
		if (l >= r) {
			return;
		}
		int k = random.nextInt(r - l + 1) + l;
		swap(a, l, k);
		ArrayList<Integer> m = partition3(a, l, r);
		randomizedQuickSort(a, l, m.get(0) - 1);
		randomizedQuickSort(a, m.get(1) + 1, r);
	}

	private ArrayList<Integer> partition3(ArrayList<T> a, int l, int r) {
		T x = a.get(l);
		int i = l + 1;
		for (; i <= r;) {
			if (x.compareTo(a.get(i)) < 0) {
				swap(a, r, i);
				r--;

			} else if (x.compareTo(a.get(i)) > 0) {
				swap(a, l, i);
				l++;
				i++;
			} else {
				i++;
			}
		}
		int m1 = l;
		int m2 = r;
		ArrayList<Integer> m = new ArrayList<>();
		m.add(m1);
		m.add(m2);
		return m;
	}

	private void swap(ArrayList<T> target, int a1, int a2) {
		T temp1 = target.get(a1);
		T temp2 = target.get(a2);
		target.remove(a1);
		target.add(a1, temp2);
		target.remove(a2);
		target.add(a2, temp1);
	}

}