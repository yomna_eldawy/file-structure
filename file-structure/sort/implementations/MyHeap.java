package eg.edu.alexu.csd.filestructure.sort.implementations;

import java.util.ArrayList;
import java.util.Collection;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

public class MyHeap<T extends Comparable<T>> implements IHeap<T> {
	private ArrayList<INode<T>> nodesList = new ArrayList<INode<T>>();

	public MyHeap() {
		INode<T> dummy = new MyNode<T>();
		nodesList.add(dummy);
	}

	@Override
	public INode<T> getRoot() {
		if (nodesList.size() > 1) {
			return nodesList.get(1);
		}
		return null;

	}

	@Override
	public int size() {
		return nodesList.size() - 1;
	}

	@Override
	public void heapify(INode<T> node) {
		int index = nodesList.indexOf(node);
		System.out.println(index);
		System.out.println(size());
		if (index < nodesList.size() / 2 && index > 1)
			heapifyDown(index);
	}

	@Override
	public T extract() {
		T root = getRoot().getValue();
		if (nodesList.size() < 2) {
			return null;
		}
		nodesList.remove(1);
		nodesList.add(1, nodesList.get(nodesList.size() - 1));
		nodesList.remove(nodesList.size() - 1);
		heapifyDown(1);
		return root;
	}

	@Override
	public void insert(T element) {
		INode<T> n = new MyNode<T>();
		n.setValue(element);
		nodesList.add(n);
		heapifyUp(nodesList.size() - 1);

	}

	@Override
	public void build(Collection<T> unordered) {
		Object unorderedArray[] = unordered.toArray();
		nodesList = new ArrayList<INode<T>>();
		INode<T> dummy = new MyNode<T>();
		nodesList.add(dummy);
		for (int i = 0; i < unordered.size(); i++) {
			try {
				insert((T) unorderedArray[i]);
			} catch (Exception e) {
			}

		}

		for (int i = 0; i <= (nodesList.size() - 1) / 2; i++) {
			((MyNode<T>) nodesList.get(i)).setLeftChild(nodesList.get(2 * i));
			((MyNode<T>) nodesList.get(2 * i)).setParent(nodesList.get(i));

			if (2 * i + 1 < nodesList.size()) {
				((MyNode<T>) nodesList.get(i)).setRightChild(nodesList
						.get(2 * i + 1));
				((MyNode<T>) nodesList.get(2 * i + 1)).setParent(nodesList
						.get(i));

			}
		}

	}

	public void heapifyUp(int i) {
		if (i > 1
				&& (nodesList.get(i).getValue().compareTo(nodesList.get(i / 2)
						.getValue())) > 0) {
			swap(i, i / 2);
			heapifyUp(i / 2);
		}
	}

	public void heapifyDown(int i) {
		if (i <= (nodesList.size() - 1) / 2) {
			if (i * 2 + 1 >= nodesList.size()) {
				if (nodesList.get(i).getValue()
						.compareTo(nodesList.get(2 * i).getValue()) < 0) {
					swap(i, i * 2);
				}
				return;
			}
			T leftValue = nodesList.get(i * 2).getValue();
			T rightValue = nodesList.get(i * 2 + 1).getValue();
			T max = rightValue;
			T min = leftValue;
			boolean leftIsBigger = false;
			if (rightValue.compareTo(leftValue) < 0) {
				max = leftValue;
				min = rightValue;
				leftIsBigger = true;
			}
			if (nodesList.get(i).getValue().compareTo(min) < 0) {
				if (leftIsBigger) {
					swap(i, i * 2);
					heapifyDown(i * 2);
				} else {
					swap(i, i * 2 + 1);
					heapifyDown(i * 2 + 1);

				}
			} else if (nodesList.get(i).getValue().compareTo(max) < 0) {
				if (leftIsBigger) {
					swap(i, i * 2);
					heapifyDown(i * 2);
				} else {
					swap(i, i * 2 + 1);
					heapifyDown(i * 2 + 1);

				}
			}
		}
	}

	public void swap(int i1, int i2) {
		INode<T> temp = nodesList.get(i1);
		nodesList.set(i1, nodesList.get(i2));
		nodesList.set(i2, temp);
	}

	public void print() {
		int currentLevel = 0;
		int nextPower = 1;
		for (int i = 1; i < nodesList.size(); i++) {
			System.out.print(nodesList.get(i).getValue() + "  ");
			if (nextPower == i) {
				System.out.println();
				currentLevel++;
				nextPower += Math.pow(2, currentLevel);
			}

		}
	}

}